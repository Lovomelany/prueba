package models;

import java.io.Serializable;

public class Producto implements Serializable{

 int id;
private String name;
double price;

 public Producto() {
this.id = 0;
this.name = "";
this.price = 0.0;
}

 public Producto(int id, String name, double price) {
this.id = id;
this.name = name;
this.price = price;
}

 public int getId() {
return id;
}

 public void setId(int id) {
this.id = id;
}

 public String getName() {
return name;
}

 public void setName(String name) {
this.name = name;
}

 public double getprice() {
return price;
}

 public void setPrice(double price) {
this.price = price;
}

}